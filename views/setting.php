<?php

/**
 * MariaDB view.
 *
 * @category   apps
 * @package    cockpit
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/cockpit/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('cockpit');

///////////////////////////////////////////////////////////////////////////////
// Show warning if cockpit is not running
///////////////////////////////////////////////////////////////////////////////

echo "<div id='cockpit_not_running' style='display:none;'>";
echo infobox_warning(lang('base_warning'), lang('cockpit_management_tool_not_accessible'));
echo "</div>";

///////////////////////////////////////////////////////////////////////////////
// Password set (change to just be the default)
///////////////////////////////////////////////////////////////////////////////

echo "<div id='cockpit_password_ok' style='display:none;'>";

$options['buttons']  = array(
    anchor_custom($url_cockpit, lang('cockpit_go_to_management_tool'), 'high', array('target' => '_blank'))
);

echo infobox_highlight(
    lang('cockpit_management_tool'),
    lang('cockpit_management_tool_help'),
    $options
);

echo form_open('cockpit');

echo form_footer();
echo form_close();

echo "</div>";
