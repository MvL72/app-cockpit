<?php

$lang['cockpit_app_name'] = 'Cockpit Server';
$lang['cockpit_app_description'] = 'Cockpit is a web-based management platform for Linux servers. It focuses on speed and security.';
$lang['cockpit_app_tooltip'] = 'The root user for the system is the same account for Cockpit.';
$lang['cockpit_cockpit'] = 'Cockpit';
$lang['cockpit_access_denied'] = 'Access is denied.';
$lang['cockpit_cockpit_storage'] = 'Cockpit Storage';
$lang['cockpit_management_tool'] = 'Cockpit Management Tool';
$lang['cockpit_management_tool_help'] = 'Follow the link to access the Cockpit management tool.';
$lang['cockpit_go_to_management_tool'] = 'Go To Cockpit Management Tool';
$lang['cockpit_management_tool_not_accessible'] = 'The Cockpit Management Tool is not available when the service is not running';
